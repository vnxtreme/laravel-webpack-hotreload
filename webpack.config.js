const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserJSPlugin = require("terser-webpack-plugin");

module.exports = {
    entry: ["./resources/js/app.js", "./resources/sass/app.scss"],
    mode: "production",
    optimization: {
        minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
        // splitChunks: {chunks: 'all'}
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: "babel-loader",
                options: { presets: ["@babel/env"] }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    // "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                loader: "url-loader?limit=100000" 
            }
        ]
    },
    resolve: { extensions: ["*", ".js", ".jsx", ".scss", ".woff", ".woff2", ".eot", ".ttf", ".svg"] },
    output: {
        path: path.resolve(__dirname, "public/js"),
        publicPath: "http://localhost:3000/js/",
        filename: "[name].js",
        chunkFilename: "[name].bundle.js"
    },
    devServer: {
        contentBase: path.join(__dirname, "public/"),
        headers: { "Access-Control-Allow-Origin": "*" },
        port: 3000,
        hotOnly: true
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new MiniCssExtractPlugin({
            filename: "[name].css"
        })
    ]
};
