Upgraded to React 16.8
1. Webpack packages
    add: react-hot-loader
    -D:
        - CSS: css-loader style-loader sass sass-loader node-sass mini-css-extract-plugin optimize-css-assets-webpack-plugin
        - JS: babel-loader terser-webpack-plugin
        - JS: @babel/cli @babel/core @babel/plugin-syntax-dynamic-import @babel/plugin-transform-runtime @babel/preset-env @babel/preset-react
        - Webpack: webpack webpack-cli webpack-dev-server
2. Touch .babelrc add {"presets": ["@babel/env", "@babel/preset-react"]}
3. Import {hot} from react-hot-loader [then] export default hot(module)(Example)
4. Add div.#example
5. Add <script src="http://localhost:3000/js/main.js"></script>
6. Yarn start