<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        {{-- <link rel="stylesheet" href="{{asset('js/main.css')}}"> --}}
        {{-- <link rel="stylesheet" href="{{asset('PlayableBanner/css/app.css')}}"> --}}
    </head>
    <body>
        <div id="playable_banner"></div>
        {{-- <script src="http://localhost:8000/js/main.js"></script> --}}
        <script src="{{asset('PlayableBanner/app.js')}}"></script>
    </body>
</html>
