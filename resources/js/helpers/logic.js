export const generateNumber = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const generateCards = (min = 1, max = 32, limit = 3) => {
    let arr = [];

    new Array(max).fill(undefined).map((number, index) => {
        let randNumber = Math.floor(Math.random() * (max - min + 1)) + min;
        
        if (arr.length < limit && arr.indexOf(randNumber) == -1) {
            arr.push(randNumber);
        }
    });

    return arr;
};
