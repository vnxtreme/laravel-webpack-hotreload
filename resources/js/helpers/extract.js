import slugify from "./slugify";

export const extractFirstParagraph = html => {
    let arrayMatches = html.match(/^([^.]+[.])/gm),
        newArray = arrayMatches.map(str => str + "..");
    return newArray.join("");
};

export const convertTitle = html => {
    return html.replace(/[^\r\n].+[:]/g, function(match) {
        let doubleDotRemoved = match.replace(/[:]/g, "");
        return `<h5>${doubleDotRemoved}</h5>`;
    });
};

export const convertData = detailsHtml => {
    let firstParagraphs = extractFirstParagraph(detailsHtml);
    let convertedTitle = convertTitle(firstParagraphs);
    return convertedTitle;
};

export const extractCardsName = data => {
    return data.split(" - ").map(name => slugify(name));
};
