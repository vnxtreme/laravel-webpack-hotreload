import React, { useState, useCallback } from "react";
import Axios from "axios";
import { CommonProvider } from "./CommonContext";
import { extractCardsName, convertData } from "../helpers/extract";
import { generateCards } from "../helpers/logic";

function GlobalState(props) {
    const [isOpen, setIsOpen] = useState(false);
    const [step, setStep] = useState(1);
    const [answerHtml, setAnswerHtml] = useState(null);
    const [cardNames, setCardNames] = useState(["ach-co", "ach-co", "ach-co"]);

    const postRequest = useCallback(
        async url => {
            let cards = generateCards();
            let params = {
                card1: cards[0],
                card2: cards[1],
                card3: cards[2],
            };
            
            var formData = new FormData();

            Object.keys(params).forEach(key => {
                formData.append(key, params[key]);
            });

            try {
                let { data } = await Axios.post(url, formData);

                //convert data
                let convertedData = convertData(data.DetailsHtml);

                //extract cards name
                let arrayCardsName = extractCardsName(data.Details);

                setCardNames(arrayCardsName);
                setAnswerHtml(convertedData);
            } catch (error) {
                console.log(error);
            }
        },
        [isOpen]
    );

    const openCardsOnclick = () => {
        if (!isOpen) {
            setTimeout(() => {
                setIsOpen(true);
            }, 200);
            _gaq.push(['_trackEvent', 'PlayableBanner', 'onClick', 'Mở bài']);
        } else {
            setStep(3);
            _gaq.push(['_trackEvent', 'PlayableBanner', 'onClick', 'Xem kết quả']);
        }
    };

    function headerOnclick() {
        if (isOpen) {
            setStep(3);
        } else {
            _gaq.push(['_trackEvent', 'PlayableBanner', 'onClick', 'Rút ngay']);
            setStep(2); //show hidden cards
        }
    }

    const stateTree = {
        isOpen,
        setIsOpen,
        step,
        setStep,
        answerHtml,
        setAnswerHtml,
        cardNames,
        setCardNames,
        headerOnclick,
        openCardsOnclick,
        postRequest
    };

    return <CommonProvider value={stateTree}>{props.children}</CommonProvider>;
}

export default GlobalState;
