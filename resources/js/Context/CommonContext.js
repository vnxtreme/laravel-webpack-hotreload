import React, {createContext} from 'react';

const CommonContext = createContext();

export const CommonProvider = CommonContext.Provider;
export const CommonConsumer = CommonContext.Consumer;

export default CommonContext;