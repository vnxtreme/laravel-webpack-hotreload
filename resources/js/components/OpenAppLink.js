import React from 'react'

export default function OpenAppLink(props) {
    function linkOnclick(){
        _gaq.push(['_trackEvent', 'PlayableBanner', 'onClick', 'Tải app']);
    }

    return (
        <a href="https://tuviphongthuy.net/tai-app-boibai.html" target="_blank" className="PlayableBanner_link" onClick={linkOnclick}>
           {props.children}
        </a>
    )
}
