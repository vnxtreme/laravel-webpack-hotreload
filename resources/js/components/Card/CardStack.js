import React, { useContext } from "react";
import CommonContext from "../../Context/CommonContext";
import Card from "./Card";

export default function CardStack() {
    const { isOpen, cardNames, openCardsOnclick } = useContext(CommonContext);

    return (
        <div className={`PlayableBanner_card-stack d-flex justify-content-center ${isOpen ? "active" : ""}`} onClick={openCardsOnclick}>
            {cardNames.map((name, i) => (
                <Card key={i} name={name} />
            ))}
        </div>
    );
}
