import React from "react";

export default function Card(props) {
    return (
        <div className="flip-card mx-1">
            <div className="flip-card-inner">
                <div className="flip-card-front">
                    <img src="https://static1.yan.vn/playable/boibaitay/images/BoiBaiTay_PlayableBanner_card-layout.jpg?v=2.0" alt="Avatar" className="PlayableBanner_img-responsive " /* style="width:300px;height:300px;" */ />
                </div>
                <div className="flip-card-back">
                    <img src={`https://static1.yan.vn/playable/boibaitay/images/playing_cards/${props.name}.jpg?v=2.0`} alt="Avatar" className="PlayableBanner_img-responsive" /* style="width:300px;height:300px;" */ />
                </div>
            </div>
        </div>
    );
}
