import React, { useContext, useEffect } from "react";
import CardStack from "../Card/CardStack";
import OpenAppLink from "../OpenAppLink";
import CommonContext from "../../Context/CommonContext";
import './styles.css';

export default function WithdrawLayout() {
    const { isOpen, postRequest, openCardsOnclick } = useContext(CommonContext);

    useEffect(() => {
        postRequest("http://tuviphongthuy.net/api/thongtinque");
    }, []);

    return (
        <div className="PlayableBanner_withdraw-layout row justify-content-center pt-1">
            <div className="PlayableBanner_header m-2">
                <div className="PlayableBanner_title">
                    <img src="https://static1.yan.vn/playable/boibaitay/images/BoiBaiTay_PlayableBanner_title.png?v=2.0" alt="PlayableBanner" className="PlayableBanner_img-responsive" />
                </div>
                <div className="PlayableBanner_note golden-text m-2">Hãy thật tĩnh tâm và tập trung vào việc đang thắc mắc, sau đó nhấn MỞ BÀI</div>
            </div>
            <div className="PlayableBanner_body vw-100">
                <CardStack />
            </div>
            <div className="PlayableBanner_footer my-3">
                <button className="PlayableBanner_btn PlayableBanner_btn-golden PlayableBanner_text-large text-uppercase px-4" onClick={openCardsOnclick}>
                    {isOpen ? "Xem kết quả" : "Mở bài"}
                </button>
                <div className="PlayableBanner_text-golden mt-2 text-center">
                    <OpenAppLink>
                        <u>Tải app, xem mỗi ngày</u>
                    </OpenAppLink>
                </div>
            </div>
        </div>
    );
}
