import { useEffect, useRef, useContext } from "react";
import CommonContext from "../Context/CommonContext";

export default function detectOutsideClick() {
    const { step, setStep } = useContext(CommonContext);
    const ref = useRef(null);

    const handleHideOnKeystroke = event => {
        if (event.key === "Escape") {
            step != 0 && setStep(1);
        }
    };

    const handleClickOutside = event => {
        if (ref.current && !ref.current.contains(event.target)) {
            step != 0 && setStep(1);
        }
    };

    useEffect(() => {
        document.addEventListener("keydown", handleHideOnKeystroke, true);
        document.addEventListener("click", handleClickOutside, true);
        return () => {
            document.removeEventListener("keydown", handleHideOnKeystroke, true);
            document.removeEventListener("click", handleClickOutside, true);
        };
    });

    return { ref };
}
