import React, { useContext } from "react";
import CommonContext from "../../Context/CommonContext";

export default function CloseButton() {
    const { setStep } = useContext(CommonContext);

    const handleCloseButton = () => {
        setStep(0);
    };

    return (
        <div className="PlayableBanner_close-button">
            <button type="button" className="PlayableBanner_button" aria-label="Close" onClick={handleCloseButton}>
                <div aria-hidden="true">&times;</div>
            </button>
        </div>
    );
}
