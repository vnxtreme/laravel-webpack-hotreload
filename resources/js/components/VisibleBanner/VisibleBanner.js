import React, { useContext } from "react";
import CommonContext from "../../Context/CommonContext";
import OpenAppLink from "../OpenAppLink";
import { generateNumber } from "../../helpers/logic";

import "./style.css";

function VisibleBanner() {
    const { headerOnclick } = useContext(CommonContext);
    const texts = [
        "Luận Tài Vận Hôm Nay",
        "Luận Tình Duyên Hôm Nay",
        "Luận Sự Nghiệp Hôm Nay",
    ];

    return (
        <div className="PlayableBanner_visible-banner PlayableBanner_background-theme row">
            <div className="col-8 PlayableBanner_left">
                <div className="row">
                    <div className="col-3 px-2 px-sm-3">
                        <OpenAppLink>
                            <img
                                src="https://static1.yan.vn/playable/boibaitay/images/BoiBaiTay_PlayableBanner_logo.png?v=2.0"
                                alt="Bói bài"
                                className="PlayableBanner_img-responsive PlayableBanner_app-logo"
                            />
                        </OpenAppLink>
                    </div>
                    <div className="col-9 align-self-center pl-0 pr-1">
                        <OpenAppLink>
                            <div className="text-white">
                                <strong>Rút Bài Tây Ba Lá,<br />{texts[generateNumber(0, 2)]}</strong>
                            </div>
                        </OpenAppLink>
                    </div>
                </div>
            </div>
            <div className="col-4 right pl-0 text-right align-self-center">
                <button className="PlayableBanner_btn PlayableBanner_btn-golden text-uppercase PlayableBanner_shine" onClick={headerOnclick}>
                    <strong>Rút ngay</strong>
                </button>
            </div>
        </div>
    );
}

export default VisibleBanner;
