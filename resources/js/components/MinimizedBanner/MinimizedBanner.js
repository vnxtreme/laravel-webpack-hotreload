import React, {useContext } from "react";
import CommonContext from "../../Context/CommonContext";

import "./style.css";

export default function MinimizedBanner() {
    const { setStep } = useContext(CommonContext);

    const minimizedBannerOnclick = () => setStep(1);

    return (
        <img src="https://static1.yan.vn/playable/boibaitay/images/COLLAGE.png?v=2.0" alt="" className="img-responsive PlayableBanner_minimzed" onClick={minimizedBannerOnclick} />
    );
}
