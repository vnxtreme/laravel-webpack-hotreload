import React, { useContext, useEffect } from "react";

import CommonContext from "../../Context/CommonContext";
import VisibleBanner from "../VisibleBanner/VisibleBanner";
import CloseButton from "../CloseButton/CloseButton";
import MinimizedBanner from "../MinimizedBanner/MinimizedBanner";

import RevealAnswer from "../RevealAnswer/RevealAnswer";
import WithdrawLayout from "../WithdrawLayout/WithdrawLayout";

import detectOutsideClick from "../detectOutsideClick";

export default function PlayableBanner() {
    const { step } = useContext(CommonContext);
    const { ref } = detectOutsideClick(true);

    useEffect(()=>{
        _gaq.push(['_trackEvent', 'PlayableBanner', 'view', 'Bói bài Tây']);
    }, [])

    const renderComponent = () => {
        switch (step) {
            case 1:
                return <VisibleBanner />;
                break;

            case 2:
                return <WithdrawLayout />;
                break;

            case 3:
                return <RevealAnswer />;
                break;

            default:
                return <MinimizedBanner />;
                break;
        }
    };

    return (
        <div ref={ref} className="PlayableBanner fixed-bottom container">
            {step > 0 && <CloseButton />}

            {renderComponent()}
        </div>
    );
}
