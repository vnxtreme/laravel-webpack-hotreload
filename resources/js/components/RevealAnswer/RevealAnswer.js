import React, { useContext } from "react";
import CommonContext from "../../Context/CommonContext";
import OpenAppLink from "../OpenAppLink";
import './style.css';

export default function RevealAnswer() {
    const { answerHtml } = useContext(CommonContext);

    return (
        <div className="PlayableBanner_withdraw-layout row justify-content-center">
            <div className="PlayableBanner_header mt-2">
                <div className="PlayableBanner_title">
                    <img src="https://static1.yan.vn/playable/boibaitay/images/BoiBaiTay_PlayableBanner_title.png?v=2.0" alt="PlayableBanner" className="PlayableBanner_img-responsive" />
                </div>
            </div>
            <div className="PlayableBanner_body vw-100">
                <div className="PlayableBanner_answer px-3">
                    <p dangerouslySetInnerHTML={{ __html: answerHtml }} />
                </div>
            </div>
            <div className="PlayableBanner_footer my-3">
                <OpenAppLink>
                    <button className="PlayableBanner_btn PlayableBanner_btn-golden text-uppercase px-4 PlayableBanner_text-large PlayableBanner_shine">Tải app để xem đầy đủ</button>
                </OpenAppLink>
            </div>
        </div>
    );
}
