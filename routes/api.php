<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('cards-reveal', function(Request $request){
    return [
        'status' => 'ok',
        'data' => [
            collect([
                'title' => 'card 1',
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa architecto perspiciatis officia aut mollitia doloribus dolorem. Harum autem quaerat quis officia voluptatibus est, sequi sit voluptate. Quod ipsum modi totam.'
            ]),
            collect([
                'title' => 'card 2',
                'description' => '2 Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa architecto perspiciatis officia aut mollitia doloribus dolorem. Harum autem quaerat quis officia voluptatibus est, sequi sit voluptate. Quod ipsum modi totam.'
            ])
        ]
    ];
});
